
var request = require("request")

// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
exports.handler = function (event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);

        /**
         * Uncomment this if statement and populate with your skill's application ID to
         * prevent someone else from configuring a skill that sends requests to this function.
         */

    // if (event.session.application.applicationId !== "") {
    //     context.fail("Invalid Application ID");
    //  }

        if (event.session.new) {
            onSessionStarted({requestId: event.request.requestId}, event.session);
        }

        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } catch (e) {
        context.fail("Exception: " + e);
    }
};

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
    // add any session init logic here
}

/**
 * Called when the user invokes the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
    getWelcomeResponse(callback)
}

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {

    var intent = intentRequest.intent
    var intentName = intentRequest.intent.name;

    // dispatch custom intents to handlers here
           if (intentName == "FreeParkingIntent") {
        handleGetInfoIntent(intent, session, callback, "43")
    }
       else if (intentName == "BusPickupIntent") {
        handleGetInfoIntent(intent, session, callback, "1")
    }
    else if (intentName == "DownloadConferenceNotesIntent") {
        handleGetInfoIntent(intent, session, callback, "2")
    } 
    else if (intentName == "WhereIsConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "3")
    }
    else if (intentName == "WhyIsMyBadgeScannedIntent") {
        handleGetInfoIntent(intent, session, callback, "4")
    }
    else if (intentName == "WVCTVIntent") {
        handleGetInfoIntent(intent, session, callback, "5")
    }
    else if (intentName == "ConferenceNotesAvailabilityIntent") {
        handleGetInfoIntent(intent, session, callback, "6")
    }
    else if (intentName == "UploadConferenceNotesIntent") {
        handleGetInfoIntent(intent, session, callback, "7")
    }
    else if (intentName == "SessionsThatDoNotHaveNotesIntent") {
        handleGetInfoIntent(intent, session, callback, "8")
    }
    else if (intentName == "ConvinceMyBossIWillBenefitFromAttendingIntent") {
        handleGetInfoIntent(intent, session, callback, "9")
    }
    else if (intentName == "WhenWillExhibitHallOpenIntent") {
        handleGetInfoIntent(intent, session, callback, "10")
    }
    else if (intentName == "WhoCanAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "11")
    }
    else if (intentName == "HowManyPeopleAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "12")
    }
    else if (intentName == "HowManyCEHoursAreOfferedIntent") {
        handleGetInfoIntent(intent, session, callback, "13")
    }
    else if (intentName == "HowDoIRegisterForAnnualConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "14")
    }
    else if (intentName == "CancellationPolicyIntent") {
        handleGetInfoIntent(intent, session, callback, "15")
    }
    else if (intentName == "CanIBringGuestsIntent") {
        handleGetInfoIntent(intent, session, callback, "16")
    }
    else if (intentName == "ChildPolicyIntent") {
        handleGetInfoIntent(intent, session, callback, "17")
    }
    else if (intentName == "ProvideChildCareIntent") {
        handleGetInfoIntent(intent, session, callback, "18")
    }
    else if (intentName == "RegisterOtherPeopleIntent") {
        handleGetInfoIntent(intent, session, callback, "19")
    }
    else if (intentName == "WhatIsWaitListIntent") {
        handleGetInfoIntent(intent, session, callback, "20")
    }
    else if (intentName == "WhatSessionsCanIAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "21")
    }
    else if (intentName == "DoISignUpForEachSessionIWantToAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "22")
    }
    else if (intentName == "IsLunchIncludedIntent") {
        handleGetInfoIntent(intent, session, callback, "23")
    }
    else if (intentName == "AreConferenceNotesIncludedIntent") {
        handleGetInfoIntent(intent, session, callback, "24")
    }
    else if (intentName == "HowDoIUseMyScheduleIntent") {
        handleGetInfoIntent(intent, session, callback, "25")
    }
    else if (intentName == "DownloadWVCMobileAppIntent") {
        handleGetInfoIntent(intent, session, callback, "26")
    }
    else if (intentName == "FreeWifiInConventionCenterIntent") {
        handleGetInfoIntent(intent, session, callback, "27")
    }
    else if (intentName == "BadgePickupIntent") {
        handleGetInfoIntent(intent, session, callback, "28")
    }
    else if (intentName == "HowDoIReceiveCECertificateIntent") {
        handleGetInfoIntent(intent, session, callback, "29")
    }
    else if (intentName == "ReserveHotelRoomIntent") {
        handleGetInfoIntent(intent, session, callback, "30")
    }
    else if (intentName == "BringMyRVIntent") {
        handleGetInfoIntent(intent, session, callback, "31")
    }
    else if (intentName == "ProvidedTransportationToTheAirportIntent") {
        handleGetInfoIntent(intent, session, callback, "32")
    }
    else if (intentName == "TransportationToFromContractedHotelsIntent") {
        handleGetInfoIntent(intent, session, callback, "33")
    }
    else if (intentName == "CheckBagsDuringConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "34")
    }
    else if (intentName == "WhatShouldIWearIntent") {
        handleGetInfoIntent(intent, session, callback, "35")
    }
         else if (intentName == "VegasWeatherIntent") {
        handleGetInfoIntent(intent, session, callback, "36")
    }
    else if (intentName == "MothersRoomIntent") {
        handleGetInfoIntent(intent, session, callback, "37")
    }
    else if (intentName == "ParticipantsWithDisabilitiesIntent") {
        handleGetInfoIntent(intent, session, callback, "38")
    }
    else if (intentName == "LostItemsAtConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "39")
    }
    else if (intentName == "BrowseAnnualConferenceScientificProgramIntent") {
        handleGetInfoIntent(intent, session, callback, "40")
    }
    else if (intentName == "DatesForNextConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "41")
    }
    else if (intentName == "WhatCanISayIntent") {
        handleGetInfoIntent(intent, session, callback, "46")
    }
    else if (intentName == "NoIntent") {
        handleGetInfoIntent(intent, session, callback, "44")
    }
    else if (intentName == "YesIntent") {
        handleGetInfoIntent(intent, session, callback, "45")
    }
    else if (intentName == "ForgotPasswordIntent") {
        handleGetInfoIntent(intent, session, callback, "47")
    }
    else {
         throw "Invalid intent"
    }
}

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
}

// ------- Skill specific logic -------

function url1() {
     return {
        url: "https://mywvctest.wvc.org/Alexa/Question",
        qs: {
            "question" : "42",
            "deviceId" : "1"
        }
    }
} 

function getWelcomeResponse(callback) {
request.get(url1(), function(error, response, body) {
        var d = JSON.parse(body.replace(/&quot;/g,'"'))
        var result = d.Answer

    var speechOutput = result

    var reprompt = "If you are unsure what questions I can answer simply say, what can I ask, and an email will be sent to you with a detailed list of questions you can ask me."

    var header = "Welcome"

    var shouldEndSession = false

    var sessionAttributes = {
        "speechOutput" : speechOutput,
        "repromptText" : reprompt
    }

    callback(sessionAttributes, buildSpeechletResponse(header, speechOutput, reprompt, shouldEndSession))
    })
      

}

function handleGetInfoIntent(intent, session, callback, question) {

    var speechOutput = "We have an error"

    getJSON(question, function(data) {
        if (data != "ERROR") {
            var speechOutput = data
        }
        if(question == "44"){
                callback(session.attributes, buildSpeechletResponse("WVC",speechOutput, "", true))
            }
            else{
                    callback(session.attributes, buildSpeechletResponse("WVC",speechOutput, "Is there anything else I can help you with", false))
                }
    })

}

function url3(question) {
     return {
        url: "https://mywvctest.wvc.org/Alexa/Question",
        qs: {
            "question" : question,
            "deviceId" : "1"
        }
    }
}

function getJSON(question, callback) {

       // HTTPS with WVC
    request.get(url3(question), function(error, response, body) {
        var d = JSON.parse(body.replace(/&quot;/g,'"'))
        var result = d.Answer
        if (result.length > 0) {
            callback(result)
        } else {
            callback("ERROR")
        }
    })
}


// ------- Helper functions to build responses for Alexa -------


function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: title,
            content: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildSpeechletResponseWithoutCard(output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    };
}

function capitalizeFirst(s) {
    return s.charAt(0).toUpperCase() + s.slice(1)
}