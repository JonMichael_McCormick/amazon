    if (intentName == "FreeParkingIntent") {
        handleGetInfoIntent(intent, session, callback, "Where Is Free Parking")
    }
    else if (intentName == "DownloadConferenceNotesIntent") {
        handleGetInfoIntent(intent, session, callback, "how do I download my conference notes")
    } 
    else if (intentName == "BusPickupIntent") {
        handleGetInfoIntent(intent, session, callback, "when will the bus pick us up for 16216")
    }
    else if (intentName == "WhereIsConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "Where is the 2018 WVC Annual Conference")
    }
    else if (intentName == "WhyIsMyBadgeScannedIntent") {
        handleGetInfoIntent(intent, session, callback, "Why is my badge being scanned")
    }
    else if (intentName == "WVCTVIntent") {
        handleGetInfoIntent(intent, session, callback, "How can I watch WVC-TV")
    }
    else if (intentName == "ConferenceNotesAvailabilityIntent") {
        handleGetInfoIntent(intent, session, callback, "How long are Conference Notes available")
    }
    else if (intentName == "UploadConferenceNotesIntent") {
        handleGetInfoIntent(intent, session, callback, "Can I upload Conference Notes to my personal or work computer? How")
    }
    else if (intentName == "SessionsThatDoNotHaveNotesIntent") {
        handleGetInfoIntent(intent, session, callback, "What sessions do not have Conference Notes")
    }
    else if (intentName == "ConvinceMyBossIWillBenefitFromAttendingIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I convince my boss that I will benefit from attending the Annual Conference")
    }
    else if (intentName == "WhenWillExhibitHallOpenIntent") {
        handleGetInfoIntent(intent, session, callback, "When will the 2018 Exhibit Hall be open")
    }
    else if (intentName == "WhoCanAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "Who can attend the WVC Annual Conference")
    }
    else if (intentName == "HowManyPeopleAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "How many people usually attend the Annual Conference")
    }
    else if (intentName == "HowManyCEHoursAreOfferedIntent") {
        handleGetInfoIntent(intent, session, callback, "How many CE hours are offered")
    }
    else if (intentName == "HowDoIRegisterForAnnualConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I register for the WVC 90th Annual Conference")
    }
    else if (intentName == "CancellationPolicyIntent") {
        handleGetInfoIntent(intent, session, callback, "What is your cancellation policy for registration and paid sessions")
    }
    else if (intentName == "CanIBringGuestsIntent") {
        handleGetInfoIntent(intent, session, callback, "Can I bring a guest")
    }
    else if (intentName == "ChildPolicyIntent") {
        handleGetInfoIntent(intent, session, callback, "What is your child policy")
    }
    else if (intentName == "ProvideChildCareIntent") {
        handleGetInfoIntent(intent, session, callback, "Does WVC provide childcare")
    }
    else if (intentName == "RegisterOtherPeopleIntent") {
        handleGetInfoIntent(intent, session, callback, "I am not going to attend the conference, but would like to register several people from my clinic to attend. How do I do this")
    }
    else if (intentName == "WhatIsWaitListIntent") {
        handleGetInfoIntent(intent, session, callback, "What does waitlist mean")
    }
    else if (intentName == "WhatSessionsCanIAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "What sessions am I able to attend")
    }
    else if (intentName == "DoISignUpForEachSessionIWantToAttendIntent") {
        handleGetInfoIntent(intent, session, callback, "Do I sign up for each session I want to attend")
    }
    else if (intentName == "IsLunchIncludedIntent") {
        handleGetInfoIntent(intent, session, callback, "Is lunch included in the registration")
    }
    else if (intentName == "AreConferenceNotesIncludedIntent") {
        handleGetInfoIntent(intent, session, callback, "Are the Conference Notes included in the registration fee")
    }
    else if (intentName == "HowDoIUseMyScheduleIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I use My Schedule")
    }
    else if (intentName == "DownloadWVCMobileAppIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I download the WVC-Connect mobile app")
    }
    else if (intentName == "FreeWifiInConventionCenterIntent") {
        handleGetInfoIntent(intent, session, callback, "Is there free WiFi offered at the Mandalay Bay South Convention Center")
    }
    else if (intentName == "BadgePickupIntent") {
        handleGetInfoIntent(intent, session, callback, "Where do I pick up my badge")
    }
    else if (intentName == "HowDoIReceiveCECertificateIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I receive my CE certificate")
    }
    else if (intentName == "ReserveHotelRoomIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I reserve my hotel room and what options do I have")
    }
    else if (intentName == "BringMyRVIntent") {
        handleGetInfoIntent(intent, session, callback, "Can I bring my RV to the Conference")
    }
    else if (intentName == "ProvidedTransportationToTheAirportIntent") {
        handleGetInfoIntent(intent, session, callback, "Is WVC providing transportation to/from McCarran International Airport")
    }
    else if (intentName == "TransportationToFromContractedHotelsIntent") {
        handleGetInfoIntent(intent, session, callback, "Is WVC providing hotel shuttle transportation to/from other contracted hotels to Mandalay Bay Convention Center")
    }
    else if (intentName == "CheckBagsDuringConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "Is there a place to check our bags during Annual Conference")
    }
    else if (intentName == "WhatShouldIWearIntent") {
        handleGetInfoIntent(intent, session, callback, "What should I wear")
    }
    else if (intentName == "MothersRoomIntent") {
        handleGetInfoIntent(intent, session, callback, "Will there be a Mother's Room")
    }
     else if (intentName == "VegasWeatherIntent") {
        handleGetInfoIntent(intent, session, callback, "What is the weather like in Las Vegas in March")
    }
    else if (intentName == "ParticipantsWithDisabilitiesIntent") {
        handleGetInfoIntent(intent, session, callback, "Participants with Disabilitie")
    }
    else if (intentName == "LostItemsAtConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "What do I do if I lose an item at conference")
    }
    else if (intentName == "BrowseAnnualConferenceScientificProgramIntent") {
        handleGetInfoIntent(intent, session, callback, "How do I browse the Annual Conference scientific program")
    }
    else if (intentName == "DatesForNextConferenceIntent") {
        handleGetInfoIntent(intent, session, callback, "What are the dates for the next conference")
    }
    else {
         throw "Invalid intent"
    }